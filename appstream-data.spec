%define gitdate 20250213
%global enable_epel 0

Summary:   Cached AppStream metadata
Name:      appstream-data
Epoch:     1
Version:   10
Release:   %{gitdate}.1%{?dist}
BuildArch: noarch
License:   CC0-1.0 AND CC-BY-1.0 AND CC-BY-SA-1.0 AND GFDL-1.1-or-later
URL:       http://people.redhat.com/rhughes/metadata/
Source1:   http://people.redhat.com/rhughes/metadata/rhel-%{version}-%{gitdate}.xml.gz
Source2:   http://people.redhat.com/rhughes/metadata/rhel-%{version}-%{gitdate}-icons.tar.gz
Source3:   https://raw.githubusercontent.com/hughsie/fedora-appstream/master/appstream-extra/adobe-flash.xml
Source4:   https://raw.githubusercontent.com/hughsie/fedora-appstream/master/appstream-extra/gstreamer-non-free.xml
Source5:   https://raw.githubusercontent.com/hughsie/fedora-appstream/master/appstream-extra/other-repos.xml

# extra applications not in RHEL
%if 0%{?enable_epel}
Source9:   http://people.redhat.com/rhughes/metadata/epel-%{version}-%{gitdate}.xml.gz
Source10:  http://people.redhat.com/rhughes/metadata/epel-%{version}-%{gitdate}-icons.tar.gz
%endif

# This is built using:
# sudo dnf makecache --enablerepo rhel-10-baseos --enablerepo rhel-10-appstream --enablerepo rhel-10-crb
# export ARCHIVE_PATH=/media
# dnf reposync --setopt=*.module_hotfixes=1 --repo rhel-10-baseos -p ${ARCHIVE_PATH}/rhel/ &> rhel-10-baseos.log
# dnf reposync --setopt=*.module_hotfixes=1 --repo rhel-10-appstream -p ${ARCHIVE_PATH}/rhel/ &> rhel-10-appstream.log
# dnf reposync --setopt=*.module_hotfixes=1 --repo rhel-10-crb -p ${ARCHIVE_PATH}/rhel/ &> rhel-10-crb.log
# dnf reposync --setopt=*.module_hotfixes=1 --repo rhel-10-ha -p ${ARCHIVE_PATH}/rhel/ &> rhel-10-ha.log
# https://github.com/hughsie/appstream-scripts/blob/master/rhel/rhel-10-candidate.sh
# then the sources need to be uploaded to people.redhat.com/rhughes/metadata/

BuildRequires: libappstream-glib

%description
This package provides the distribution specific AppStream metadata required
for the GNOME and KDE software centers.

%install

DESTDIR=%{buildroot} appstream-util install-origin rhel-%{version} %{SOURCE1} %{SOURCE2}
%if 0%{?enable_epel}
DESTDIR=%{buildroot} appstream-util install-origin epel-%{version} %{SOURCE9} %{SOURCE10}
%endif
DESTDIR=%{buildroot} appstream-util install %{SOURCE3} %{SOURCE4} %{SOURCE5}

# Move to the "correct" path for appstream 0.16 / 1.0
mv %{buildroot}%{_datadir}/app-info %{buildroot}%{_datadir}/swcatalog
mv %{buildroot}%{_datadir}/swcatalog/xmls %{buildroot}%{_datadir}/swcatalog/xml

%files
%attr(0644,root,root) %{_datadir}/swcatalog/xml/*
%{_datadir}/swcatalog/icons/rhel-%{version}/*/*.png
%if 0%{?enable_epel}
%{_datadir}/swcatalog/icons/epel-%{version}/*/*.png
%endif
%dir %{_datadir}/swcatalog
%dir %{_datadir}/swcatalog/icons
%dir %{_datadir}/swcatalog/icons/rhel-%{version}/64x64
%dir %{_datadir}/swcatalog/icons/rhel-%{version}/128x128
%if 0%{?enable_epel}
%dir %{_datadir}/swcatalog/icons/epel-%{version}/64x64
%dir %{_datadir}/swcatalog/icons/epel-%{version}/128x128
%endif
%dir %{_datadir}/swcatalog/xml

%changelog
%autochangelog
